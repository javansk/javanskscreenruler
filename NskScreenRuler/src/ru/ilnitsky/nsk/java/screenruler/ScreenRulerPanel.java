package ru.ilnitsky.nsk.java.screenruler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;

/**
 * Панель для экранной линейки на основе Swing
 * Created by Mike on 16.04.2017.
 */
public class ScreenRulerPanel extends JPanel implements ScreenRulerInterface {
    private JWindow window;

    private BufferedImage verticalImage;
    private BufferedImage verticalInvertedImage;
    private BufferedImage verticalFromCenterImage;
    private BufferedImage horizontalImage;
    private BufferedImage horizontalInvertedImage;
    private BufferedImage horizontalFromCenterImage;
    private Graphics2D verticalGraphics;
    private Graphics2D verticalInvertedGraphics;
    private Graphics2D verticalFromCenterGraphics;
    private Graphics2D horizontalGraphics;
    private Graphics2D horizontalInvertedGraphics;
    private Graphics2D horizontalFromCenterGraphics;

    private Color backgroundColor = Color.WHITE;
    private Color textColor = Color.BLACK;

    private Cursor cursorHand = new Cursor(Cursor.HAND_CURSOR);
    private Cursor cursorMove = new Cursor(Cursor.MOVE_CURSOR);
    private Cursor cursorLeft = new Cursor(Cursor.E_RESIZE_CURSOR);
    private Cursor cursorRight = new Cursor(Cursor.W_RESIZE_CURSOR);
    private Cursor cursorTop = new Cursor(Cursor.N_RESIZE_CURSOR);
    private Cursor cursorBottom = new Cursor(Cursor.S_RESIZE_CURSOR);

    private int rulerWidth = 80;
    private int internalWidth = 60;
    private int lineLength;
    private int middleLineLength;
    private int bigLineLength;

    private int middleStep = 8;
    private int bigStep = 32;
    private int resizeLength = 16;

    private boolean isVertical;
    private RulerDirection direction;

    private boolean isDragged;
    private boolean isBeginResize;
    private boolean isEndResize;

    private Point startWindowPosition;
    private Point startCursorPosition;
    private Dimension startWindowSize;
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    public ScreenRulerPanel(JWindow window, boolean isVertical) {
        this.isVertical = isVertical;
        this.window = window;

        verticalImage = new BufferedImage(rulerWidth, (int) screenSize.getHeight(), TYPE_INT_RGB);
        horizontalImage = new BufferedImage((int) screenSize.getWidth(), rulerWidth, TYPE_INT_RGB);
        verticalInvertedImage = new BufferedImage(rulerWidth, (int) screenSize.getHeight(), TYPE_INT_RGB);
        horizontalInvertedImage = new BufferedImage((int) screenSize.getWidth(), rulerWidth, TYPE_INT_RGB);
        verticalFromCenterImage = new BufferedImage(rulerWidth, (int) screenSize.getHeight(), TYPE_INT_RGB);
        horizontalFromCenterImage = new BufferedImage((int) screenSize.getWidth(), rulerWidth, TYPE_INT_RGB);

        verticalGraphics = (Graphics2D) verticalImage.getGraphics();
        horizontalGraphics = (Graphics2D) horizontalImage.getGraphics();
        verticalInvertedGraphics = (Graphics2D) verticalInvertedImage.getGraphics();
        horizontalInvertedGraphics = (Graphics2D) horizontalInvertedImage.getGraphics();
        verticalFromCenterGraphics = (Graphics2D) verticalFromCenterImage.getGraphics();
        horizontalFromCenterGraphics = (Graphics2D) horizontalFromCenterImage.getGraphics();

        lineLength = (rulerWidth - internalWidth) / 2;
        middleLineLength = lineLength + 4;
        bigLineLength = lineLength + 12;

        setImages();

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    startWindowSize = window.getSize();
                    startWindowPosition = window.getLocationOnScreen();
                    startCursorPosition = e.getLocationOnScreen();

                    isDragged = true;
                    if (isVertical) {
                        if (startCursorPosition.y >= startWindowPosition.y && startCursorPosition.y < startWindowPosition.y + resizeLength) {
                            isBeginResize = true;
                            isDragged = false;
                        } else if (startCursorPosition.y >= startWindowPosition.y + startWindowSize.height - resizeLength
                                && startCursorPosition.y <= startWindowPosition.y + startWindowSize.height - 1) {
                            isEndResize = true;
                            isDragged = false;
                        }
                    } else {
                        if (startCursorPosition.x >= startWindowPosition.x && startCursorPosition.x < startWindowPosition.x + resizeLength) {
                            isBeginResize = true;
                            isDragged = false;
                        } else if (startCursorPosition.x >= startWindowPosition.x + startWindowSize.width - resizeLength
                                && startCursorPosition.x <= startWindowPosition.x + startWindowSize.width - 1) {
                            isEndResize = true;
                            isDragged = false;
                        }
                    }

                    if (isDragged) {
                        setCursor(cursorMove);
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    isDragged = false;
                    isBeginResize = false;
                    isEndResize = false;

                    if (isVertical) {
                        if (window.getHeight() > screenSize.getHeight()) {
                            window.setSize(window.getWidth(), (int) screenSize.getHeight());
                        }
                    } else {
                        if (window.getWidth() > screenSize.getWidth()) {
                            window.setSize((int) screenSize.getWidth(), window.getHeight());
                        }
                    }
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    if (direction == RulerDirection.DIRECT) {
                        direction = RulerDirection.INVERTED;
                    } else if (direction == RulerDirection.INVERTED) {
                        direction = RulerDirection.FROM_CENTER;
                    } else if (direction == RulerDirection.FROM_CENTER) {
                        direction = RulerDirection.DIRECT;
                    }
                    repaint();
                } else if (e.getButton() == MouseEvent.BUTTON3) {
                    resize();
                }
            }
        });

        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);

                int x = e.getX();
                int y = e.getY();
                if (isVertical) {
                    if (y >= 0 && y < resizeLength) {
                        setCursor(cursorTop);
                    } else if (y >= getHeight() - resizeLength && y <= getHeight() - 1) {
                        setCursor(cursorBottom);
                    } else {
                        setCursor(cursorHand);
                    }
                } else {
                    if (x >= 0 && x < resizeLength) {
                        setCursor(cursorLeft);
                    } else if (x >= getWidth() - resizeLength && x <= getWidth() - 1) {
                        setCursor(cursorRight);
                    } else {
                        setCursor(cursorHand);
                    }
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);

                if (isDragged || isBeginResize || isEndResize) {
                    Point position = e.getLocationOnScreen();
                    int dx = position.x - startCursorPosition.x;
                    int dy = position.y - startCursorPosition.y;
                    if (isDragged) {
                        window.setLocation(startWindowPosition.x + dx, startWindowPosition.y + dy);
                    } else if (isVertical) {
                        if (isBeginResize) {
                            window.setSize(startWindowSize.width, startWindowSize.height - dy);
                            window.setLocation(startWindowPosition.x, startWindowPosition.y + dy);
                        } else if (isEndResize) {
                            window.setSize(startWindowSize.width, startWindowSize.height + dy);
                        }
                    } else {
                        if (isBeginResize) {
                            window.setSize(startWindowSize.width - dx, startWindowSize.height);
                            window.setLocation(startWindowPosition.x + dx, startWindowPosition.y);
                        } else if (isEndResize) {
                            window.setSize(startWindowSize.width + dx, startWindowSize.height);
                        }
                    }
                }
            }
        });

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);

                int newX = window.getX();
                int newY = window.getY();
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    newX++;
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    newX--;
                }
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    newY--;
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    newY++;
                }
                window.setLocation(newX, newY);
            }
        });
    }

    private void resize() {
        if (direction == RulerDirection.DIRECT) {
            if (isVertical) {
                if (window.getHeight() == screenSize.getHeight()) {
                    window.setSize(window.getWidth(), (int) (screenSize.getHeight() / 2));
                } else {
                    window.setSize(window.getWidth(), (int) screenSize.getHeight());
                }
            } else {
                if (window.getWidth() == screenSize.getWidth()) {
                    window.setSize((int) (screenSize.getWidth() / 2), window.getHeight());
                } else {
                    window.setSize((int) screenSize.getWidth(), window.getHeight());
                }
            }
        } else if (direction == RulerDirection.INVERTED) {
            if (isVertical) {
                if (window.getHeight() == screenSize.getHeight()) {
                    int newHeight = (int) (screenSize.getHeight() / 2);
                    window.setSize(window.getWidth(), newHeight);
                    window.setLocation(window.getLocationOnScreen().x, window.getLocationOnScreen().y + newHeight);
                } else {
                    int oldHeight = window.getHeight();
                    window.setSize(window.getWidth(), (int) screenSize.getHeight());
                    window.setLocation(window.getLocationOnScreen().x, window.getLocationOnScreen().y - (window.getHeight() - oldHeight));
                }
            } else {
                if (window.getWidth() == screenSize.getWidth()) {
                    int newWidth = (int) (screenSize.getWidth() / 2);
                    window.setSize(newWidth, window.getHeight());
                    window.setLocation(window.getLocationOnScreen().x + newWidth, window.getLocationOnScreen().y);
                } else {
                    int oldWidth = window.getWidth();
                    window.setSize((int) screenSize.getWidth(), window.getHeight());
                    window.setLocation(window.getLocationOnScreen().x - (window.getWidth() - oldWidth), window.getLocationOnScreen().y);
                }
            }
        } else if (direction == RulerDirection.FROM_CENTER) {
            if (isVertical) {
                if (window.getHeight() == screenSize.getHeight()) {
                    int newHeight = (int) (screenSize.getHeight() / 2);
                    window.setSize(window.getWidth(), newHeight);
                    window.setLocation(window.getLocationOnScreen().x, window.getLocationOnScreen().y + newHeight / 2);
                } else {
                    int oldHeight = window.getHeight();
                    window.setSize(window.getWidth(), (int) screenSize.getHeight());
                    window.setLocation(window.getLocationOnScreen().x, window.getLocationOnScreen().y - (window.getHeight() - oldHeight) / 2);
                }
            } else {
                if (window.getWidth() == screenSize.getWidth()) {
                    int newWidth = (int) (screenSize.getWidth() / 2);
                    window.setSize(newWidth, window.getHeight());
                    window.setLocation(window.getLocationOnScreen().x + newWidth / 2, window.getLocationOnScreen().y);
                } else {
                    int oldWidth = window.getWidth();
                    window.setSize((int) screenSize.getWidth(), window.getHeight());
                    window.setLocation(window.getLocationOnScreen().x - (window.getWidth() - oldWidth) / 2, window.getLocationOnScreen().y);
                }
            }
        }
    }

    private void setImages() {
        setVerticalImage(verticalImage, verticalGraphics, RulerDirection.DIRECT);
        setVerticalImage(verticalInvertedImage, verticalInvertedGraphics, RulerDirection.INVERTED);
        setVerticalImage(verticalFromCenterImage, verticalFromCenterGraphics, RulerDirection.FROM_CENTER);
        setHorizontalImage(horizontalImage, horizontalGraphics, RulerDirection.DIRECT);
        setHorizontalImage(horizontalInvertedImage, horizontalInvertedGraphics, RulerDirection.INVERTED);
        setHorizontalImage(horizontalFromCenterImage, horizontalFromCenterGraphics, RulerDirection.FROM_CENTER);
    }

    private void setVerticalImage(BufferedImage verticalImage, Graphics2D verticalGraphics, RulerDirection direction) {
        verticalGraphics.setColor(backgroundColor);
        verticalGraphics.fillRect(0, 0, verticalImage.getWidth(), verticalImage.getHeight());

        int imageSize = verticalImage.getHeight();
        int rightBegin = rulerWidth - lineLength;
        int middleRightBegin = rulerWidth - middleLineLength;
        int bigRightBegin = rulerWidth - bigLineLength;

        int half = rulerWidth / 2;
        int length1 = half - 4;
        int length2 = half - 8;
        int length3 = half - 12;
        int length4 = half - 16;
        int length5 = half - 20;

        verticalGraphics.setColor(textColor);

        if (direction == RulerDirection.DIRECT) {
            for (int i = 0; i < imageSize; i += 2) {
                if (i % bigStep == 0) {
                    verticalGraphics.drawLine(0, i, bigLineLength - 1, i);
                    verticalGraphics.drawLine(bigRightBegin, i, rulerWidth - 1, i);
                    if (i > 999) {
                        verticalGraphics.drawString(Integer.toString(i), length4, i + 5);
                    } else if (i > 99) {
                        verticalGraphics.drawString(Integer.toString(i), length3, i + 5);
                    } else if (i > 9) {
                        verticalGraphics.drawString(Integer.toString(i), length2, i + 5);
                    } else {
                        verticalGraphics.drawString(Integer.toString(i), length1, i + 5);
                    }
                } else if (i % middleStep == 0) {
                    verticalGraphics.drawLine(0, i, middleLineLength - 1, i);
                    verticalGraphics.drawLine(middleRightBegin, i, rulerWidth - 1, i);
                } else {
                    verticalGraphics.drawLine(0, i, lineLength - 1, i);
                    verticalGraphics.drawLine(rightBegin, i, rulerWidth - 1, i);
                }
            }
        } else if (direction == RulerDirection.INVERTED) {
            for (int i = imageSize - 1; i >= 0; i -= 2) {
                int number = imageSize - 1 - i;
                if (number % bigStep == 0) {
                    verticalGraphics.drawLine(0, i, bigLineLength - 1, i);
                    verticalGraphics.drawLine(bigRightBegin, i, rulerWidth - 1, i);
                    if (number > 999) {
                        verticalGraphics.drawString(Integer.toString(number), length4, i + 5);
                    } else if (number > 99) {
                        verticalGraphics.drawString(Integer.toString(number), length3, i + 5);
                    } else if (number > 9) {
                        verticalGraphics.drawString(Integer.toString(number), length2, i + 5);
                    } else {
                        verticalGraphics.drawString(Integer.toString(number), length1, i + 5);
                    }
                } else if (number % middleStep == 0) {
                    verticalGraphics.drawLine(0, i, middleLineLength - 1, i);
                    verticalGraphics.drawLine(middleRightBegin, i, rulerWidth - 1, i);
                } else {
                    verticalGraphics.drawLine(0, i, lineLength - 1, i);
                    verticalGraphics.drawLine(rightBegin, i, rulerWidth - 1, i);
                }
            }
        } else if (direction == RulerDirection.FROM_CENTER) {
            int center = imageSize / 2;
            verticalGraphics.drawLine(0, center, bigLineLength - 1, center);
            verticalGraphics.drawLine(bigRightBegin, center, rulerWidth - 1, center);
            verticalGraphics.drawString(Integer.toString(0), length1, center + 6);

            for (int i = 2; i <= center; i += 2) {
                if (i % bigStep == 0) {
                    verticalGraphics.drawLine(0, center + i, bigLineLength - 1, center + i);
                    verticalGraphics.drawLine(bigRightBegin, center + i, rulerWidth - 1, center + i);
                    verticalGraphics.drawLine(0, center - i, bigLineLength - 1, center - i);
                    verticalGraphics.drawLine(bigRightBegin, center - i, rulerWidth - 1, center - i);
                    if (i > 999) {
                        verticalGraphics.drawString("+" + i, length5, center + i + 5);
                        verticalGraphics.drawString("-" + i, length5, center - i + 5);
                    } else if (i > 99) {
                        verticalGraphics.drawString("+" + i, length4, center + i + 5);
                        verticalGraphics.drawString("-" + i, length4, center - i + 5);
                    } else if (i > 9) {
                        verticalGraphics.drawString("+" + i, length3, center + i + 5);
                        verticalGraphics.drawString("-" + i, length3, center - i + 5);
                    } else {
                        verticalGraphics.drawString("+" + i, length2, center + i + 5);
                        verticalGraphics.drawString("-" + i, length2, center - i + 5);
                    }
                } else if (i % middleStep == 0) {
                    verticalGraphics.drawLine(0, center + i, middleLineLength - 1, center + i);
                    verticalGraphics.drawLine(middleRightBegin, center + i, rulerWidth - 1, center + i);
                    verticalGraphics.drawLine(0, center - i, middleLineLength - 1, center - i);
                    verticalGraphics.drawLine(middleRightBegin, center - i, rulerWidth - 1, center - i);
                } else {
                    verticalGraphics.drawLine(0, center + i, lineLength - 1, center + i);
                    verticalGraphics.drawLine(rightBegin, center + i, rulerWidth - 1, center + i);
                    verticalGraphics.drawLine(0, center - i, lineLength - 1, center - i);
                    verticalGraphics.drawLine(rightBegin, center - i, rulerWidth - 1, center - i);
                }
            }
        }

    }

    private void setHorizontalImage(BufferedImage horizontalImage, Graphics2D horizontalGraphics, RulerDirection direction) {
        horizontalGraphics.setColor(backgroundColor);
        horizontalGraphics.fillRect(0, 0, horizontalImage.getWidth(), horizontalImage.getHeight());

        int imageSize = horizontalImage.getWidth();
        int rightBegin = rulerWidth - lineLength;
        int middleRightBegin = rulerWidth - middleLineLength;
        int bigRightBegin = rulerWidth - bigLineLength;

        int textPosition = rulerWidth / 2 + 4;

        horizontalGraphics.setColor(textColor);

        if (direction == RulerDirection.DIRECT) {
            for (int i = 0; i < imageSize; i += 2) {
                if (i % bigStep == 0) {
                    horizontalGraphics.drawLine(i, 0, i, bigLineLength - 1);
                    horizontalGraphics.drawLine(i, bigRightBegin, i, rulerWidth - 1);
                    horizontalGraphics.drawString(Integer.toString(i), i - 4, textPosition);
                } else if (i % middleStep == 0) {
                    horizontalGraphics.drawLine(i, 0, i, middleLineLength - 1);
                    horizontalGraphics.drawLine(i, middleRightBegin, i, rulerWidth - 1);
                } else {
                    horizontalGraphics.drawLine(i, 0, i, lineLength - 1);
                    horizontalGraphics.drawLine(i, rightBegin, i, rulerWidth - 1);
                }
            }
        } else if (direction == RulerDirection.INVERTED) {
            for (int i = imageSize - 1; i >= 0; i -= 2) {
                int number = imageSize - 1 - i;
                if (number % bigStep == 0) {
                    horizontalGraphics.drawLine(i, 0, i, bigLineLength - 1);
                    horizontalGraphics.drawLine(i, bigRightBegin, i, rulerWidth - 1);
                    horizontalGraphics.drawString(Integer.toString(number), i - 4, textPosition);
                } else if (number % middleStep == 0) {
                    horizontalGraphics.drawLine(i, 0, i, middleLineLength - 1);
                    horizontalGraphics.drawLine(i, middleRightBegin, i, rulerWidth - 1);
                } else {
                    horizontalGraphics.drawLine(i, 0, i, lineLength - 1);
                    horizontalGraphics.drawLine(i, rightBegin, i, rulerWidth - 1);
                }
            }
        } else if (direction == RulerDirection.FROM_CENTER) {
            int shift1 = -4;
            int shift2 = -8;
            int shift3 = -12;
            int shift4 = -16;
            int shift5 = -20;
            int shift;

            int center = imageSize / 2;
            horizontalGraphics.drawLine(center, 0, center, bigLineLength - 1);
            horizontalGraphics.drawLine(center, bigRightBegin, center, rulerWidth - 1);
            horizontalGraphics.drawString(Integer.toString(0), center + shift1, textPosition);

            for (int i = 2; i <= center; i += 2) {
                if (i % bigStep == 0) {
                    horizontalGraphics.drawLine(center + i, 0, center + i, bigLineLength - 1);
                    horizontalGraphics.drawLine(center + i, bigRightBegin, center + i, rulerWidth - 1);
                    horizontalGraphics.drawLine(center - i, 0, center - i, bigLineLength - 1);
                    horizontalGraphics.drawLine(center - i, bigRightBegin, center - i, rulerWidth - 1);
                    if (i > 999) {
                        shift = shift5;
                    } else if (i > 99) {
                        shift = shift4;
                    } else if (i > 9) {
                        shift = shift3;
                    } else {
                        shift = shift2;
                    }
                    horizontalGraphics.drawString("+" + i, center + i + shift, textPosition);
                    horizontalGraphics.drawString("-" + i, center - i + shift, textPosition);
                } else if (i % middleStep == 0) {
                    horizontalGraphics.drawLine(center + i, 0, center + i, middleLineLength - 1);
                    horizontalGraphics.drawLine(center + i, middleRightBegin, center + i, rulerWidth - 1);
                    horizontalGraphics.drawLine(center - i, 0, center - i, middleLineLength - 1);
                    horizontalGraphics.drawLine(center - i, middleRightBegin, center - i, rulerWidth - 1);
                } else {
                    horizontalGraphics.drawLine(center + i, 0, center + i, lineLength - 1);
                    horizontalGraphics.drawLine(center + i, rightBegin, center + i, rulerWidth - 1);
                    horizontalGraphics.drawLine(center - i, 0, center - i, lineLength - 1);
                    horizontalGraphics.drawLine(center - i, rightBegin, center - i, rulerWidth - 1);
                }
            }
        }

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (isVertical) {
            if (direction == RulerDirection.DIRECT) {
                if (verticalImage != null) {
                    g.drawImage(verticalImage, 0, 0, backgroundColor, this);
                }
            } else if (direction == RulerDirection.INVERTED) {
                if (verticalInvertedImage != null) {
                    g.drawImage(verticalInvertedImage, 0, window.getHeight() - verticalInvertedImage.getHeight(), backgroundColor, this);
                }
            } else if (direction == RulerDirection.FROM_CENTER) {
                if (verticalFromCenterImage != null) {
                    g.drawImage(verticalFromCenterImage, 0, (window.getHeight() - verticalFromCenterImage.getHeight()) / 2, backgroundColor, this);
                }
            }
        } else {
            if (direction == RulerDirection.DIRECT) {
                if (horizontalImage != null) {
                    g.drawImage(horizontalImage, 0, 0, backgroundColor, this);
                }
            } else if (direction == RulerDirection.INVERTED) {
                if (horizontalInvertedImage != null) {
                    g.drawImage(horizontalInvertedImage, window.getWidth() - horizontalInvertedImage.getWidth(), 0, backgroundColor, this);
                }
            } else if (direction == RulerDirection.FROM_CENTER) {
                if (horizontalFromCenterImage != null) {
                    g.drawImage(horizontalFromCenterImage, (window.getWidth() - horizontalFromCenterImage.getWidth()) / 2, 0, backgroundColor, this);
                }
            }

        }
    }

    public int getMaxWidth() {
        if (isVertical) {
            return rulerWidth;
        } else {
            return (int) screenSize.getWidth();
        }
    }

    public int getMaxHeight() {
        if (isVertical) {
            return (int) screenSize.getHeight();
        } else {
            return rulerWidth;
        }
    }

    @Override
    public void setVertical(boolean isVertical) {
        this.isVertical = isVertical;
    }

    @Override
    public void setDirection(RulerDirection direction) {
        this.direction = direction;
    }

    @Override
    public void setScale(int middleStep, int bigStep) {
        this.middleStep = middleStep;
        this.bigStep = bigStep;

        setImages();
    }

    @Override
    public void setColor(Color backgroundColor, Color textColor) {
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;

        setImages();
    }

    @Override
    public void setBackgroundColor(Color color) {
        this.backgroundColor = color;

        setImages();
    }

    @Override
    public void setTextColor(Color color) {
        this.textColor = color;

        setImages();
    }

    @Override
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public Color getTextColor() {
        return textColor;
    }

    @Override
    public boolean isVertical() {
        return isVertical;
    }

    @Override
    public RulerDirection getDirection() {
        return direction;
    }
}
