package ru.ilnitsky.nsk.java.screenruler;

import java.awt.*;

/**
 * Интерфейс для экранной линейки
 * Created by Mike on 16.04.2017.
 */
public interface ScreenRulerInterface {
    void setVertical(boolean isVertical);

    void setDirection(RulerDirection direction);

    void setScale(int middleStep, int bigStep);

    void setColor(Color backgroundColor, Color textColor);

    void setBackgroundColor(Color color);

    void setTextColor(Color color);

    Color getBackgroundColor();

    Color getTextColor();

    boolean isVertical();

    RulerDirection getDirection();
}
