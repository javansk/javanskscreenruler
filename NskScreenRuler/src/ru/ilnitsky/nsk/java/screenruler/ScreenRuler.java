package ru.ilnitsky.nsk.java.screenruler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Экранная линейка на основе Swing
 * Created by Mike on 16.04.2017.
 */
public class ScreenRuler extends JFrame {
    private final ScreenRulerWindow verticalRuler;
    private final ScreenRulerWindow horizontalRuler;
    private final JMenuBar menuBar;

    private final JPanel buttonPanel = new JPanel();
    private final JRadioButton verticalButton = new JRadioButton("Вертикальная");
    private final JRadioButton horizontalButton = new JRadioButton("Горизонтальная");
    private final ButtonGroup rulerGroup = new ButtonGroup();

    private final JButton leftButton = new JButton("Влево");
    private final JButton rightButton = new JButton("Вправо");
    private final JButton downButton = new JButton("Вниз");
    private final JButton upButton = new JButton("Вверх");

    private final int[] middleStep = {4, 6, 12, 14, 10, 4, 8, 16, 6, 12, 18, 10, 8, 12, 16, 10, 10, 12, 8, 8, 20, 20};
    private final int[] bigStep = {24, 24, 24, 28, 30, 32, 32, 32, 36, 36, 36, 40, 48, 48, 48, 50, 60, 60, 64, 80, 80, 100};

    private ScreenRuler() {
        super("NskScreenRuler - экранная пиксельная линейка");

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exitDialog();
            }
        });

        Image image = new ImageIcon(getClass().getResource("/ru/ilnitsky/nsk/java/screenruler/NskScreenRuler.png")).getImage();
        setIconImage(image);

        verticalRuler = new ScreenRulerWindow(this, true, RulerDirection.DIRECT);
        horizontalRuler = new ScreenRulerWindow(this, false, RulerDirection.DIRECT);

        menuBar = new JMenuBar();
        initMenuBar();
        setJMenuBar(menuBar);

        initButtonPanel();
        add(buttonPanel, BorderLayout.CENTER);

        pack();
        Dimension dimension = getSize();
        setMinimumSize(dimension);

        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void initMenuBar() {
        JMenu menuFile = new JMenu("Файл");
        JMenuItem menuExit = new JMenuItem("Выход");
        menuExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        menuExit.addActionListener(e -> exitDialog());
        menuFile.add(menuExit);

        JMenu menuHorizontal = getRulerMenu(horizontalRuler, "Горизонтальная");

        JMenu menuVertical = getRulerMenu(verticalRuler, "Вертикальная");

        JMenu menuAbout = new JMenu("О программе");
        JMenuItem menuAbout2 = new JMenuItem("О программе");
        menuAbout2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        menuAbout2.addActionListener(e ->
                JOptionPane.showMessageDialog(this,
                        new String[]{"NskScreenRuler",
                                "Экранная пиксельная линейка",
                                "Графический интерфейс на основе Swing",
                                "М.Ильницкий, Новосибирск, 2017"},
                        "О программе",
                        JOptionPane.INFORMATION_MESSAGE)
        );
        menuAbout.add(menuAbout2);

        menuBar.add(menuFile);
        menuBar.add(menuVertical);
        menuBar.add(menuHorizontal);
        menuBar.add(menuAbout);
    }

    private JMenu getRulerMenu(ScreenRulerWindow window, String text) {
        JMenu menu = new JMenu(text);

        JCheckBoxMenuItem visibleItem = new JCheckBoxMenuItem("Отображать");
        visibleItem.setSelected(true);
        menu.add(visibleItem);
        visibleItem.addActionListener(e -> {
            if (visibleItem.isSelected()) {
                window.setVisible(true);
            } else {
                window.setVisible(false);
            }
        });

        JCheckBoxMenuItem alwaysOnTopItem = new JCheckBoxMenuItem("Поверх всех окон");
        menu.add(alwaysOnTopItem);
        alwaysOnTopItem.addActionListener(e -> {
            if (alwaysOnTopItem.isSelected()) {
                window.setAlwaysOnTop(true);
            } else {
                window.setAlwaysOnTop(false);
            }
        });

        JCheckBoxMenuItem opacityItem = new JCheckBoxMenuItem("Полупрозрачная");
        menu.add(opacityItem);
        opacityItem.addActionListener(e -> {
            if (opacityItem.isSelected()) {
                window.setOpacity(0.6f);
            } else {
                window.setOpacity(1f);
            }
            window.repaint();
        });

        menu.addSeparator();

        JMenuItem findItem = new JMenuItem("Найти");
        menu.add(findItem);
        findItem.addActionListener(e -> {
            if (!window.isVisible()) {
                window.setVisible(true);
            }
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int x = ((int) screenSize.getWidth() - window.getWidth()) / 2;
            int y = ((int) screenSize.getHeight() - window.getHeight()) / 2;
            window.setLocation(x, y);
        });

        menu.addSeparator();

        ButtonGroup stepGroup = new ButtonGroup();
        JRadioButtonMenuItem[] stepMenu = new JRadioButtonMenuItem[bigStep.length];
        for (int i = 0; i < bigStep.length; i++) {
            stepMenu[i] = new JRadioButtonMenuItem("Шаг: " + bigStep[i] + " / " + middleStep[i]);
            stepGroup.add(stepMenu[i]);
            menu.add(stepMenu[i]);

            Integer index = i;
            stepMenu[i].addActionListener(e -> window.setScale(middleStep[index], bigStep[index]));
        }
        stepMenu[6].doClick();

        menu.addSeparator();

        JMenuItem defaultColorItem = new JMenuItem("Цвета по умолчанию");
        menu.add(defaultColorItem);
        defaultColorItem.addActionListener(e -> window.setColor(Color.WHITE, Color.BLACK));

        JMenuItem backgroundColorItem = new JMenuItem("Цвет фона");
        menu.add(backgroundColorItem);
        backgroundColorItem.addActionListener(e -> {
            Color color = JColorChooser.showDialog(this, "Выбор цвета фона", window.getBackgroundColor());
            if (color != null) {
                window.setBackgroundColor(color);
            }
        });

        JMenuItem textColorItem = new JMenuItem("Цвет текста");
        menu.add(textColorItem);
        textColorItem.addActionListener(e -> {
            Color color = JColorChooser.showDialog(this, "Выбор цвета текста", window.getTextColor());
            if (color != null) {
                window.setTextColor(color);
            }
        });

        return menu;
    }

    private void exitDialog() {
        try {
            Object options[] = {"Да", "Нет"};
            int result = JOptionPane.showOptionDialog(this,
                    "Завершить программу?",
                    "Подтверждение завершения",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

            if (result == JOptionPane.OK_OPTION) {
                System.exit(0);
            }
        } catch (Exception exception) {
            exception.getStackTrace();
        }
    }

    private void initButtonPanel() {
        buttonPanel.setLayout(new GridLayout(2, 3));

        buttonPanel.add(verticalButton);
        buttonPanel.add(upButton);
        buttonPanel.add(horizontalButton);

        buttonPanel.add(leftButton);
        buttonPanel.add(downButton);
        buttonPanel.add(rightButton);

        rulerGroup.add(verticalButton);
        rulerGroup.add(horizontalButton);
        horizontalButton.setSelected(true);

        leftButton.addActionListener(l -> {
            if (horizontalButton.isSelected() && horizontalRuler.isVisible()) {
                Point position = horizontalRuler.getLocationOnScreen();
                horizontalRuler.setLocation(position.x - 1, position.y);
            } else if (verticalButton.isSelected() && verticalRuler.isVisible()) {
                Point position = verticalRuler.getLocationOnScreen();
                verticalRuler.setLocation(position.x - 1, position.y);
            }
        });

        rightButton.addActionListener(l -> {
            if (horizontalButton.isSelected() && horizontalRuler.isVisible()) {
                Point position = horizontalRuler.getLocationOnScreen();
                horizontalRuler.setLocation(position.x + 1, position.y);
            } else if (verticalButton.isSelected() && verticalRuler.isVisible()) {
                Point position = verticalRuler.getLocationOnScreen();
                verticalRuler.setLocation(position.x + 1, position.y);
            }
        });

        downButton.addActionListener(l -> {
            if (horizontalButton.isSelected() && horizontalRuler.isVisible()) {
                Point position = horizontalRuler.getLocationOnScreen();
                horizontalRuler.setLocation(position.x, position.y + 1);
            } else if (verticalButton.isSelected() && verticalRuler.isVisible()) {
                Point position = verticalRuler.getLocationOnScreen();
                verticalRuler.setLocation(position.x, position.y + 1);
            }
        });

        upButton.addActionListener(l -> {
            if (horizontalButton.isSelected() && horizontalRuler.isVisible()) {
                Point position = horizontalRuler.getLocationOnScreen();
                horizontalRuler.setLocation(position.x, position.y - 1);
            } else if (verticalButton.isSelected() && verticalRuler.isVisible()) {
                Point position = verticalRuler.getLocationOnScreen();
                verticalRuler.setLocation(position.x, position.y - 1);
            }
        });

        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                onKeyPressed(e);
            }
        });
        buttonPanel.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                onKeyPressed(e);
            }
        });
        leftButton.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                onKeyPressed(e);
            }
        });
        rightButton.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                onKeyPressed(e);
            }
        });
        downButton.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                onKeyPressed(e);
            }
        });
        upButton.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                onKeyPressed(e);
            }
        });

        buttonPanel.setVisible(true);
    }

    private void onKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            leftButton.doClick();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            rightButton.doClick();
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            downButton.doClick();
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            upButton.doClick();
        }
    }

    public static void main(String[] args) {
        ScreenRuler screenRuler = new ScreenRuler();
        screenRuler.setVisible(true);
    }
}
