package ru.ilnitsky.nsk.java.screenruler;

import javax.swing.*;
import java.awt.*;

/**
 * Окно для экранной линейки на основе Swing
 * Created by Mike on 16.04.2017.
 */
public class ScreenRulerWindow extends JWindow implements ScreenRulerInterface {
    private final ScreenRulerPanel panel;

    public ScreenRulerWindow(JFrame invisibleFrame, boolean isVertical, RulerDirection direction) {
        super(invisibleFrame);

        panel = new ScreenRulerPanel(this, isVertical);
        add(panel, BorderLayout.CENTER);

        panel.setDirection(direction);
        panel.setVisible(true);
        setMaxSize();

        setFocusableWindowState(true);
        panel.setFocusable(true);
        panel.setRequestFocusEnabled(true);
        panel.setFocusCycleRoot(true);
        setVisible(true);
    }

    private void setMaxSize() {
        setSize(panel.getMaxWidth(), panel.getMaxHeight());
    }

    @Override
    public void setVertical(boolean isVertical) {
        panel.setVertical(isVertical);
        setMaxSize();
        repaint();
    }

    @Override
    public void setDirection(RulerDirection direction) {
        panel.setDirection(direction);
        repaint();
    }

    @Override
    public void setScale(int middleStep, int bigStep) {
        panel.setScale(middleStep, bigStep);
        repaint();
    }

    @Override
    public void setColor(Color backgroundColor, Color textColor) {
        panel.setColor(backgroundColor, textColor);
        repaint();
    }

    @Override
    public void setBackgroundColor(Color color) {
        panel.setBackgroundColor(color);
        repaint();
    }

    @Override
    public void setTextColor(Color color) {
        panel.setTextColor(color);
        repaint();
    }

    @Override
    public Color getBackgroundColor() {
        return panel.getBackgroundColor();
    }

    @Override
    public Color getTextColor() {
        return panel.getTextColor();
    }

    @Override
    public boolean isVertical() {
        return panel.isVertical();
    }

    @Override
    public RulerDirection getDirection() {
        return panel.getDirection();
    }
}
