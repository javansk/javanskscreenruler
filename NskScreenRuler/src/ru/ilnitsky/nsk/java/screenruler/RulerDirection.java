package ru.ilnitsky.nsk.java.screenruler;

/**
 * Направление разметки экранной линейки
 * Created by UserLabView on 26.04.17.
 */
public enum RulerDirection {
    DIRECT,
    INVERTED,
    FROM_CENTER
}
